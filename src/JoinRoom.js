import { React, useState } from "react";
import { useHMSActions } from "@100mslive/react-sdk";

function JoinRoom() {
  const ENDPOINT =
    process.env.REACT_APP_TOKEN_ENDPOINT ||
    `https://prod-in2.100ms.live/hmsapi/vishal-livestream-1557.app.100ms.live/`;
  const ROOM_ID = process.env.REACT_APP_ROOM_ID || "640c57a6d3a3e81681e6664e";
  const hmsActions = useHMSActions();

  const [username, setUsername] = useState("");
  const [selectedRole, setSelectedRole] = useState("broadcaster");
  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log("handle submit called!");
    const response = await fetch(`${ENDPOINT}api/token`, {
      method: "POST",
      body: JSON.stringify({
        user_id: `${Date.now()}`,
        role: selectedRole, //broadcaster, hls-viewer
        type: "app",
        room_id: ROOM_ID,
      }),
    });
    const { token } = await response.json();
    console.log("token : ", token);
    // Joining the room
    hmsActions.join({
      userName: username,
      authToken: token,
    });
  };

  return (
    <form className="join" onSubmit={handleSubmit}>
      <input
        type="text"
        required
        placeholder="Enter name"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
      />
      <select
        type="text"
        required
        value={selectedRole}
        onChange={(e) => setSelectedRole(e.target.value)}
        placeholder="Select Role"
      >
        <option>broadcaster</option>
        <option>hls-viewer</option>
      </select>
      <button>Join</button>
    </form>
  );
}

export default JoinRoom;
